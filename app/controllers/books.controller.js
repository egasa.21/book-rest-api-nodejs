const { books } = require('../models');
const db = require('../models')
const Book = db.books;

// Create and save a book
exports.create = (req, res)=>{
    // validate request
    if(!req.body.title){
        res.status(400).send({message: "Content cannot be empty!"})
        return;
    }

    // create a book
    const book = new Book({
        title: req.body.title,
        author: req.body.author,
        released: req.body.released
    });

    // Save book in the database
    book
        .save(book)
        .then(data =>{
            res.send(data);
        })
        .catch(err=>{
            res.status(500).send({
                message: 
                err.message || "Some error occureed while creating the book"
            });
        });
}

// retrieve all books from the database
exports.findAll = (req, res)=>{
    const title = req.query.title;
    var condition = title ? {title: {$regex: new RegExp(title), $options: "i"}}  : {};

    Book.find(condition)
        .then(data =>{
            res.send(data);
        })
        .catch(err =>{
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving book"
            });
        });
};

// find a single book from the database with an Id
exports.findOne = (req, res)=>{
    const id = req.params.id;

    Books.findById(id)
        .then(data =>{
            if(!data)
                res.status(404).send({message: "Not found book with id " + id});
                else res.send(data);
        })
        .catch(err => {
            res
            .status(500)
            .send({ message: "Error retrieving Tutorial with id=" + id });
        })
}

// update a book by the id in the request
exports.update = (req, res)=>{
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Book.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
        .then(data=>{
            if(!data){
                res.status(404).send({
                    message:  `Cannot update Book with id=${id}. Maybe  book not found`
                })
            }else res.send({message: "Update successfully"})
        })
        .catch(err=>{
            res.status(500).send({
                message: "Error updating book with id= " + id
            });
        });
    
}

// deleted a bookwith the specified id in the request
exports.delete = (req, res)=>{
    const id = req.params.id;

    Book.findByIdAndRemove(id)
        .then(data=>{
            if(!data){
                res.status(404).send({
                    message: "Books not found"
                });
            }else {
                res.send({
                    message: "Delete successfully"
                });
            }
        })
        .catch(err=>{
            res.status(500).send({
                message: "Could not delete book with id " +id
            })
        })
}

// delete all books from the tutorial
exports.deleteAll = (req, res)=>{
    Book.deleteMany({})
        .then(data =>{
            res.send({
                message:`${data.deletedCount} Books were deleted successfully` 
            });
        })
        .catch(err=>{
            res.send(500).send({
                message:
                err.message || "Some error occurred while removing all books"
            });
        });
};

