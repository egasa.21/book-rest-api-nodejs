const { mongoose } = require(".");

module.exports= mongoose =>{
   var schema = mongoose.Schema(
       {
           title: String,
           author: String,
           released: String
       },
       {timestamps: true}
   );

   schema.method("toJSON", function(){
       const{__v, __id, ...object} = this.toObject();
       object.id = __id;
       return object;
   });

   const Book = mongoose.model("book", schema);
   return Book;
};