module.exports = app =>{
    const books = require('../controllers/books.controller');
    var router = require('express').Router();

    // Create new book
    router.post('/', books.create);

    // retrieve all books
    router.get('/', books.findAll);

    // retrieve a singgle book with id
    router.get('/:id', books.findOne);

    // update book with id
    router.put('/:id', books.update);

    // delete a book with id
    router.delete('/:id', books.delete);

    // delete all book
    router.delete('/', books.deleteAll);

    app.use('/api/books', router);
}