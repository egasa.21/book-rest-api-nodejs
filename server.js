const express = require('express'),
      cors = require('cors');

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse request of content-type - application/json
app.use(express.json());

// parse request of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({extended: true}));

const db = require('./app/models');
db.mongoose
    .connect(db.url, {
        useNewUrlParser : true,
        useUnifiedTopology: true
    })
    .then(()=>{
        console.log("Connected to the database");
    })
    .catch(err =>{
        console.log("Cannot connect to the database!", err);
        process.exit();
    })

// route
app.get('/', (req,res)=>{
    res.json({message: "Welcome to the jungle"});

});
require("./app/routes/books.routes")(app);

const PORT = process.env.PORT|| 8088;
app.listen(PORT,()=>{
    console.log("Listenig on port 8088");
})